#
# Be sure to run `pod lib lint PaySDKs.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PaySDKs'
  s.version          = '0.0.1.0'
  s.summary          = '支付宝框架的非官方封装'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'http://git.oschina.net/kid_wm/PaySDKs'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'wood203' => 'stronger_wm@aliyun.com' }
  s.source           = { :git => 'https://git.oschina.net/kid_wm/PaySDKs.git', :tag => s.version.to_s }
  s.social_media_url = 'http://blog.csdn.net/reborn_wood'

  s.ios.deployment_target = '8.0'

  s.requires_arc = true

  s.resources = "**/*.bundle"
  s.vendored_frameworks = "**/*.framework"

  s.frameworks = 'SystemConfiguration', 'CoreTelephony', 'QuartzCore', 'CoreText'  , 'CoreGraphics', 'UIKit', 'Foundation', 'CFNetwork', 'CoreMotion'
  s.libraries = 'c++', 'z'


end
