# PaySDKs

[![CI Status](http://img.shields.io/travis/wood203/PaySDKs.svg?style=flat)](https://travis-ci.org/wood203/PaySDKs)
[![Version](https://img.shields.io/cocoapods/v/PaySDKs.svg?style=flat)](http://cocoapods.org/pods/PaySDKs)
[![License](https://img.shields.io/cocoapods/l/PaySDKs.svg?style=flat)](http://cocoapods.org/pods/PaySDKs)
[![Platform](https://img.shields.io/cocoapods/p/PaySDKs.svg?style=flat)](http://cocoapods.org/pods/PaySDKs)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PaySDKs is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PaySDKs"
```

## Author

wood203, stronger_wm@aliyun.com

## License

PaySDKs is available under the MIT license. See the LICENSE file for more info.

